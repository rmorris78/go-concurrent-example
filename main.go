package main

import (
	"fmt"
	"math/rand"
	"time"

	petname "github.com/dustinkirkland/golang-petname"
	"gitlab.com/rmorris78/go-concurrent-example/pkg/boolgen"
)

// URLInfo is a container for the response
type URLInfo struct {
	url      string
	time     float64
	response bool
}

//
func callAPI(url string, responseChannel chan URLInfo) {
	sleepSecs := float64(rand.Intn(100)) / float64(10)
	fmt.Println(fmt.Sprintf("running %v - %.2f", url, sleepSecs))
	time.Sleep(time.Duration(sleepSecs) * time.Second)
	fmt.Println(fmt.Sprintf("done with %v - %.2f", url, sleepSecs))
	responseChannel <- URLInfo{url, sleepSecs, boolgen.New().Bool()}
}

// The extra set of parentheses here are the return type. You can give the return value a name,
// in this case +domainMapping+ and use that name in the function body. Then you don't need to specify
// what actually gets returned, you've already defined it here.
func waitForAPICalls(responseChannel chan URLInfo, numberOfUrls int) (urlMapping []URLInfo) {
	returnedCount := 0
	for {
		urlMapping = append(urlMapping, <-responseChannel)
		returnedCount++

		if returnedCount >= numberOfUrls {
			break
		}
	}

	return
}

func main() {
	concurrency := 5
	responseChannel := make(chan URLInfo, concurrency)
	urls := []string{}
	numUrls := 20
	for i := 1; i < numUrls; i++ {
		urls = append(urls, petname.Generate(2, "-"))
	}
	for _, url := range urls {
		go callAPI(url, responseChannel)

	}
	urlMapping := waitForAPICalls(responseChannel, len(urls))
	for _, x := range urlMapping {
		fmt.Printf("%s %.2f %t\n", x.url, x.time, x.response)
	}

}
